import React from 'react'
import {BrowserRouter, Switch} from 'react-router-dom'
import { PublicRoute } from './routes/PublicRoute'

import { Home } from './pages/home/Home'
import { Header } from './components/header/Header'
import Icon from './assets/images/upn_icon.png'

export const App = ()=> {
  return (
    <BrowserRouter>
      <Header src={Icon} text='UPNVIRTUAL'/>
      <Switch>
        <PublicRoute exact path='/' isAuthenticated={false} component={Home}/>
      </Switch>
    </BrowserRouter>
  )
}
