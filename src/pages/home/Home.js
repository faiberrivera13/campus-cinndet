import React from 'react'

import { PageContainer } from '../../components/container/PageContainer'
import { BigCard, LittleCard } from '../../components/home/Card'
import { Text } from '../../components/home/Text'
import BigCardimg from '../../assets/images/imageBigCard.png'
import bienestarIcon from '../../assets/images/bienestarIcon.png'
import BibliotecaIcon from '../../assets/images/BibliotecaIcon.png'
import DirectorioIcon from '../../assets/images/DirectorioIcon.png'
import EgresadosIcon from '../../assets/images/EgresadosIcon.png'
import FacultadesIcon from '../../assets/images/FacultadesIcon.png'
import NoticiasIcon from '../../assets/images/NoticiasIcon.png'
import webIcon from '../../assets/images/webIcon.png'
import littleLeftCardImage from '../../assets/images/littleLeftCardImage.png'
import littleRightCardImage from '../../assets/images/littleRightCardImage.png'
import moodleIcon from '../../assets/images/moodleIcon.png'
import siganIcon from '../../assets/images/siganIcon.png'
import outlookIcon from '../../assets/images/outlookIcon.png'
import emisoraIcon  from '../../assets/images/podcastIcon.png'
import podcastIcon from '../../assets/images/emisoraIcon.png'
import cinndetIcon from '../../assets/images/cinndetIcon.png'


const data = {
    title:'Bienvenidos al Campus Virtual de la Universidad Pedagógica Nacional',
    description:'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. ',
    bigCardImage:BigCardimg,
    bigCardTextImage:'La U',
    bigCard:{
        title:'Página web',
        subTitle:'www.pedagogica.edu.co/',
        icon:webIcon,
        link:()=>window.open('http://www.pedagogica.edu.co/')
    },
    bigNormalCards:[
        {
            id:0, 
            title:'Bienestar', 
            icon:bienestarIcon,
            link:()=>window.open('http://www.pedagogica.edu.co/'),
        },
        {
            id:1, 
            title:'Noticias', 
            icon:NoticiasIcon,
            link:()=>window.open('http://www.pedagogica.edu.co/'),
        },
        {
            id:2, 
            title:'Biblioteca', 
            icon:BibliotecaIcon,
            link:()=>window.open('http://bienestar.pedagogica.edu.co/'),
        },
        {
            id:3, 
            title:'Directorio', 
            icon:DirectorioIcon,
            link:()=>window.open('http://directorio.pedagogica.edu.co/'),
        },
        {
            id:4, 
            title:'Egresados', 
            icon:EgresadosIcon,
            link:()=>window.open('http://centroegresados.pedagogica.edu.co/'),
        },
        {
            id:5, 
            title:'Facultades', 
            icon:FacultadesIcon,
            link:()=>window.open('http://www.pedagogica.edu.co/'),
        },
    ],
    littleLeftCardImage:littleLeftCardImage,
    littleLeftCardTextImage:'Mi espacio',
    littleRightCardImage:littleRightCardImage,
    littleRightCardTextImage:'Explora',
    leftCard:{
        title:'MOODLE UPN',
        icon:moodleIcon,
        link:()=>window.open('https://upnvirtual.pedagogica.edu.co/')
    },
    leftNormalCards:[
        {
            id:0, 
            title:'Sigan', 
            icon:siganIcon,
            link:()=>window.open('http://sigan.pedagogica.edu.co/sigan/'),
        },
        {
            id:1, 
            title:'Outlook', 
            icon:outlookIcon,
            link:()=>window.open('https://outlook.office.com/mail/inbox'),
        },
    ],
    rightCard:{
        title:'(Centro de Innovación, Desarrollo Educativo y Tecnológico)',
        icon:cinndetIcon,
        link:()=>window.open('http://cinndet.upn.edu.co/')
    },
    rightNormalCards:[
        {
            id:0, 
            title:'Podcast', 
            icon:podcastIcon,
            link:()=>window.open('http://radio.pedagogica.edu.co/podcast/'),
        },
        {
            id:1, 
            title:'Emisora', 
            icon:emisoraIcon,
            link:()=>window.open('http://radio.pedagogica.edu.co/'),
        },
    ]
}


export const Home = () => {
    return (
        <PageContainer>
            <Text title={data.title} fontFamily='Montserrat-Regular' fontSize='32px' color='#005F98'/>
            <Text title={data.description} fontFamily='Montserrat-Regular' fontSize='21px' color='#303030'/>
            <BigCard 
                image={data.bigCardImage}
                bigCardTextImage={data.bigCardTextImage}
                bigNormalCards={data.bigNormalCards}
                bigCard={data.bigCard}/>
                <LittleCard
                    leftImage={data.littleLeftCardImage}
                    leftTextImage={data.littleLeftCardTextImage}
                    rightImage={littleRightCardImage}
                    rightTextImage={data.littleRightCardTextImage}
                    leftCard={data.leftCard}
                    leftNormalCards={data.leftNormalCards}
                    rightCard={data.rightCard}
                    rightNormalCards={data.rightNormalCards}/>
        </PageContainer>
    )
}