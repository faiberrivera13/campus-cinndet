import styled from 'styled-components'

const TextComponent = styled.p`
    font-family: ${props => props.fontFamily};
    font-size: ${props => props.fontSize};
    color: ${props => props.color};
`
export const Text = (props)=>{
    return (
        <TextComponent 
            fontFamily={props.fontFamily}
            fontSize={props.fontSize}
            color={props.color}>
            {props.title}
        </TextComponent>
    )
}