import styled from 'styled-components'

export const ImageBigCard = styled.img`
    width: 328px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
    @media screen and (max-width: 1024px) {
        width:187px;
        height:300px;
        align-self: start;
        justify-self: start;
        text-align:center;
        display: grid;
        padding-bottom:150px;
    }
    @media screen and (max-width: 767px) {
        width:187px;
        height:auto;
        display: grid;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:325px;
        height:auto;
        display: grid;
    }
`
export const ImageLittleLeftCard = styled.img`
    width: 172px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
`
export const ImageLittleRightCard = styled.img`
    width: 172px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
`
export const CardIcon = styled.img`
    width: ${props => props.width};
    height: ${props => props.height};
    object-fit: contain;
`