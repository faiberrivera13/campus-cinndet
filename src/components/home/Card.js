import styled from 'styled-components'
import { 
    CardIcon, 
    ImageBigCard, 
    ImageLittleLeftCard, 
    ImageLittleRightCard } from './Image'
import { Text } from './Text'

// Big Card
const CardComponent = styled.section`
    border: ${props => props.border};
    width: ${props => props.width};
    height: 200px;
    border-radius: 15px;
    display: grid;
    align-items: center;
    grid-auto-flow: column;
    
    @media screen and (max-width: 1024px) {
        width:600px;
        height:auto;
        margin-bottom:10px;
    }

    @media screen and (max-width: 767px) {
        width:600px;
        height:auto;
        margin-bottom:10px;
    }
 //falta
    @media screen and (max-width: 600px) {
        display:grid;
        grid-template-rows:repeat(2, minmax(150px, auto));
        width:350px;
        height:auto;
        margin-bottom:10px;
        grid-gap:20px;
    }

`
const CardSection = styled.section`
    justify-self: start;
    display: grid;
    align-items: center;
    justify-content: center;
    width: ${props => props.width};
    height: ${props => props.height};

    @media screen and (max-width: 1024px) {
        width:165px;
        height:auto;
        align-items: center;
        justify-content: center;
        font-size:20px;
    }

    @media screen and (max-width: 767px) {
        width:165px;
        height:auto;
        align-items: center;
        justify-content: center;
        font-size:20px;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:100%;
        height:auto;
        font-size:0.6rem;
    }
`

//crea las row en el contenedor grande
const CardMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two Four Six'
                        'One Three Five Seven';
    grid-template-rows: repeat(3, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    padding-top: 20px;
    width: ${props => props.width};
    height: ${props => props.height};

    @media screen and (max-width: 1024px) {
        justify-self: start;
        display:grid;
        text-align:center;
        align-items: center;
        justify-content: center;
        width:100%;
        height:auto;
        align-items: center;
        justify-content: center;
        grid-template-areas:'One Two'
                            'One Three '
                            'Four Six'
                            'Five Seven';
        grid-template-rows: repeat(4, minmax(70px, auto));
        padding:10px;
        padding-left:10px;
    }

    @media screen and (max-width: 767px) {
        justify-self: start;
        display:grid;
        text-align:center;
        align-items: center;
        justify-content: center;
        width:100%;
        height:auto;
        align-items: center;
        justify-content: center;
        grid-template-areas:'One Two'
                            'One Three '
                            'Four Six'
                            'Five Seven';
        grid-template-rows: repeat(2, minmax(70px, auto));
        padding:10px;
        padding-left:10px;
    }
 //falta
    @media screen and (max-width: 600px) {
        justify-self: start;
        display:grid;
        text-align:center;
        align-items: center;
        justify-content: center;
        width:100%;
        height:auto;
        align-items: center;
        justify-content: center;
        grid-template-areas:'One One'
                            'One One '
                            'Four Six'
                            'Five Seven'
                            'eigth nine';
        grid-template-rows: repeat(4, minmax(50px, auto));
        padding:10px;
        padding-left:10px;
    }
`
const BigCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: #099CB0;
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 230px;
    height: 150px;
    padding: 10px;
    cursor: pointer;
    
    @media screen and (max-width: 1024px) {
        width:187px;
        height:auto;
        display: grid;
    }
    @media screen and (max-width: 767px) {
        width:187px;
        height:auto;
        display: grid;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:325px;
        height:auto;
        display: grid;
    }
`
const BigNormalCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-content: center;
    align-items: center;
    grid-auto-flow:column;
    column-gap: 10px;
    width: 187px;
    height: 70px;
    cursor: pointer;
     //falta
     @media screen and (max-width: 600px) {
        text-align:center;
        justify-content:center;
        align-items:center;
        width:155px;
        height:70px;
        display: grid;
        grid-auto-flow:row;
    }
`
export const BigCard = (props)=>{
    return(
        <CardComponent 
            width='fit-content'
            border='solid 4px #099CB0'>
            <CardSection width='328px' height='192px'>
                <ImageBigCard src={props.image}/> 
                <Text
                    title={props.bigCardTextImage}
                    fontFamily='Montserrat-Regular'
                    color='white'
                    fontSize='36px'/>
            </CardSection>
            <CardMenuSection width='880px' height='192px'>
                <BigCardIcon gridArea='One' onClick={props.bigCard.link}>
                    <CardIcon 
                        src={props.bigCard.icon}
                        width='70px'
                        height='70px'/>
                    <Text 
                        title={props.bigCard.title}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='1.3rem'/>
                    <Text 
                        title={props.bigCard.subTitle}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='0.8rem'/>
                </BigCardIcon>
                {props.bigNormalCards.map((bigNormaCard)=>(
                    <BigNormalCardIcon
                        key={bigNormaCard.id}
                        gridArea={bigNormaCard.gridArea}
                        backgroundColor='#099CB0'>
                        <Text 
                            title={bigNormaCard.title}
                            fontFamily='Montserrat-Regular'
                            color='white'
                            fontSize='22px'/>
                        <CardIcon
                            src={bigNormaCard.icon}
                            width='44px'
                            height='38px'/>
                    </BigNormalCardIcon>
                ))}
            </CardMenuSection>
        </CardComponent>
    )
}
// Little Cards

const CardComponentLeft = styled.section`
    border: ${props => props.border};
    width: ${props => props.width};
    height: 200px;
    border-radius: 15px;
    display: grid;
    align-items: center;
    justify-content: end;
    grid-auto-flow: column;
    column-gap: 20px;

    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
    }

    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
    }
 //falta
    @media screen and (max-width: 600px) {
        justify-content:center;
        align-items:center;
        text-align:center;
        width:100%;
        display:grid;
        grid-template-rows: repeat(2, minmax(70px, auto));
        height:auto;
        font-size:0.8rem;
        padding:10px;
        grid-gap:10px;
    }
`

const CardComponentRight = styled.section`
    border: ${props => props.border};
    width: ${props => props.width};
    height: 200px;
    border-radius: 15px;
    display: grid;
    align-items: center;
    justify-content: end;
    grid-auto-flow: column;
    column-gap: 20px;

    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
    }

    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
    }
 //falta
 @media screen and (max-width: 600px) {
        justify-content:center;
        align-items:center;
        text-align:center;
        width:100%;
        display:grid;
        grid-template-rows: repeat(2, minmax(70px, auto));
        height:auto;
        font-size:0.8rem;
        padding:10px;
        grid-gap:10px;
    }
`
const CardContainerComponent = styled.div`
    display: inline-grid;
    grid-auto-flow: column;
    justify-content: center;
    align-items: center;
    column-gap: 20px;
    width: 100%;
    background-color:none;
    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
        grid-auto-flow: row;
        grid-gap:20px;
    }

    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
        grid-auto-flow: row;
        grid-gap:20px;
        background-color:none;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:425px;
        height:auto;
        grid-auto-flow: row;
        grid-gap:20px;
        justify-content:center;
        align-items:center;
        text-align:center;
        background-color:none;
    }
`
const LittleImageCardSection = styled.section`
    justify-self: start;
    display: grid;
    align-items: center;
    justify-content: center;
    width: 172px;
    height: 192px;
`
const CardLeftMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two'
                        'One Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};
    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
    }
    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:100%;
        height:auto;
        font-size:0.8rem;
    }
`
const CardRightMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Three'
                        'Two Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};

    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
    }
    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
    }
 //falta
    @media screen and (max-width: 600px) {
        width:100%;
        height:auto;
        font-size:0.8rem;
    }
`
const NormlaCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 186px;
    height: 150px;
    padding: 10px;
    cursor: pointer;
    @media screen and (max-width: 600px) {
        justify-content:center;
        align-items:center;
        text-align:center;
    }
`
export const LittleCard = (props)=>{
    return(
        <CardContainerComponent>
            <CardComponentLeft
                width='602px'
                border='solid 4px #005F98'>
                <LittleImageCardSection>
                    <ImageLittleLeftCard src={props.leftImage}/> 
                    <Text
                        title={props.leftTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='36px'/>
                </LittleImageCardSection>
                <CardLeftMenuSection>
                    <NormlaCardIcon 
                        backgroundColor='#005F98'
                        gridArea='One' 
                        onClick={props.leftCard.link}>
                        <Text 
                            title={props.leftCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='22px'/>
                        <CardIcon 
                            src={props.leftCard.icon}
                            width='100px'
                            height='100px'/>
                    </NormlaCardIcon>
                    {props.leftNormalCards.map((leftNormalCard)=>(
                        <BigNormalCardIcon
                            key={leftNormalCard.id}
                            gridArea={leftNormalCard.gridArea}
                            backgroundColor='#005F98'>
                            <Text 
                                title={leftNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='22px'/>
                            <CardIcon
                                src={leftNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                </CardLeftMenuSection>
            </CardComponentLeft>
            <CardComponentRight 
                width='602px'
                border='solid 4px #08A68E'>
                <CardRightMenuSection>
                    {props.rightNormalCards.map((rightNormalCard)=>(
                        <BigNormalCardIcon
                            key={rightNormalCard.id}
                            gridArea={rightNormalCard.gridArea}
                            backgroundColor='#08A68E'>
                            <Text 
                                title={rightNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='22px'/>
                            <CardIcon
                                src={rightNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                    <NormlaCardIcon 
                        backgroundColor='#08A68E'
                        gridArea='Three' 
                        onClick={props.rightCard.link}>
                        <CardIcon 
                            src={props.rightCard.icon}
                            width='150px'
                            height='40px'/>
                        <Text 
                            title={props.rightCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='14px'/>
                    </NormlaCardIcon>
                </CardRightMenuSection>
                <LittleImageCardSection width='602px' height='192px'>
                    <ImageLittleRightCard src={props.rightImage}/> 
                    <Text
                        title={props.rightTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='36px'/>
                </LittleImageCardSection>
            </CardComponentRight>
        </CardContainerComponent>
    )
}
