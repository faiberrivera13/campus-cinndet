import React from 'react'
import styled from 'styled-components'

const PageContainerComponent = styled.div`
    margin: 20px auto;
    height: auto;
    width: 1250px;
    display: grid;
    row-gap: 20px;
    align-content: start;
    justify-items: center;
    text-align: center;

    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
        font-size:0.8rem;
    }

    @media screen and (max-width: 767px) {
        width:100%;
        height:auto;
        font-size:0.8rem;
    }
 
    @media screen and (max-width: 600px) {
        text-align:center;
        justify-content:center;
        align-items:center;
        width:100%;
        height:auto;
        font-size:0.8rem;
    }
`
export const PageContainer = (props) => {
    return (
        <PageContainerComponent>
            {props.children}
        </PageContainerComponent>
    )
}