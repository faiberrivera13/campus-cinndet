import styled from 'styled-components'

export const HeaderContainer = styled.div`
    margin: 0 auto;
    max-width: 1280px;
    height: 85px;
    display: grid;
    align-items: center;
    justify-content: start;
    grid-auto-flow: column;
    column-gap: 21.5px;

   @media screen and (max-width: 1024px) {
        margin: 0 auto;
        max-width: 100%;
        height: 85px;
        display: grid;
        align-items: center;
        justify-content: start;
        grid-auto-flow: column;
        column-gap: 21.5px;
    }

    @media screen and (max-width: 767px) {
        margin: 0 auto;
        max-width: 100%;
        height: 85px;
        display: grid;
        align-items: center;
        justify-content: start;
        grid-auto-flow: column;
        column-gap: 21.5px;
    }
    @media screen and (max-width: 600px) {
        margin: 0 auto;
        max-width: 100%;
        height: 85px;
        display: grid;
        align-items: center;
        justify-content: start;
        grid-auto-flow: column;
        column-gap: 21.5px;
    }
    
`