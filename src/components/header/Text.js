import styled from 'styled-components'

export const Text = styled.p`
    font-family: 'Montserrat-Extra-Bold';
    font-size: 22px;
    color: whitesmoke;
    z-index: 2;
`