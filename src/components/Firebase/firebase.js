import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD72OEWAa_ETC22h8LAHRKwtTpz3s1gaRE",
  authDomain: "campus-cinndet.firebaseapp.com",
  projectId: "campus-cinndet",
  storageBucket: "campus-cinndet.appspot.com",
  messagingSenderId: "28623263424",
  appId: "1:28623263424:web:c0885c8c5c22268b5fbf25",
  measurementId: "G-KYGNFDH8SQ"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = "BBeVHiyoTJpW4cBpa78McwodBlVTQQDV0ruUlpQ9H4QrrlXzgHVmMfHJW-ssllfxpz7ieFeMfJJ-AcHs6yssvNE";
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
